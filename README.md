# Les Serruriers - Tutoriel SSH pour administrateur

## 1- Téléchargement des logiciels

### PuTTY

Aller sur [https://putty.org/](https://putty.org/).

![putty_dl_01](img/putty_dl_01.png)  
Cliquer sur `here`.

![putty_dl_02](img/putty_dl_02.png)  
Choisir le fichier d'installation correspondant à votre OS/architecture.
Pour un PC récent sous Windows 10 (donc normalement 64 bits), le fichier `putty-64bit-x.xx-installer.msi`
devrait faire l'affaire.

Une fois le fichier téléchargé, installer PuTTY.

### FileZilla

Aller sur [https://filezilla-project.org/](https://filezilla-project.org/)

![filezilla_dl_01](img/filezilla_dl_01.png)  
Cliquer sur `Download FileZilla Client`

![filezilla_dl_02](img/filezilla_dl_02.png)  
Attention, ici **il ne faut pas** cliquer sur le bouton vert `Download FileZilla Client`
mais plutôt sur le lien `Show additional download options`. En effet, le setup sur le bouton vert contient
des logiciels de pub à l'installation dont il faut se méfier, contrairement à celui que nous allons télécharger
qui est *normalement* propre.

![filezilla_dl_03](img/filezilla_dl_03.png)  
Choisir le fichier d'installation correspondant à votre OS/architecture.
Pour un PC récent sous Windows 10 (donc normalement 64 bits), le fichier `FileZilla_x.xx.x_win64-setup.exe`
devrait faire l'affaire.

Une fois le fichier téléchargé, installer FileZilla.

## 2- Génération de la clé SSH

Il est nécessaire d'avoir terminé la partie 1 de ce tutoriel (installation de PuTTY) pour réaliser cette partie 2.

![puttygen_setup_01](img/puttygen_setup_01.png)  
Lancer PuTTYgen, le mieux est de rechercher `putty` dans la barre de recherche Windows 10 et de sélectionner PuTTYgen
dans les résultats. Sinon il est toujours possible d'aller chercher le programme dans la liste des programmes installés.

![puttygen_setup_02](img/puttygen_setup_02.png)  
Une fois PuTTYgen lancé :  
1. S'assurer que RSA est bien sélectionné dans la partie `Type of key to generate`
2. Rentrer `4096` dans la case `Number of bits in a generated key`
3. Cliquer sur `Generate`

![puttygen_setup_03](img/puttygen_setup_03.png)  
Bouger la souris comme indiqué afin de générer la clé.

![puttygen_setup_04](img/puttygen_setup_04.png)  
Une fois la clé générée :  
1. Mettre votre pseudonyme dans la case `Key comment`
2. Optionnel - Mettre un mot de passe sur la clé. Ce mot de passe sera réclamé à chaque connexion au serveur
en utilisant PuTTY ou FileZilla. **Il faut** mettre un mot de passe si la clé est sur un PC partagé/utilisé par plusieurs personnes
ou si la clé SSH générée est enregistrée sur un suport amovible (clé USB/disque dur externe). Dans les autres cas, cela
n'est pas obligatoire mais **fortement recommandé**. Remplir les champs `Key passphrase` et `Confirm passphrase`
avec le mot de passe de votre choix pour le paramétrer.
3. Cliquer sur `Save private key` et sauvegarder le fichier sous le nom `cle_privee.ppk`. Il faut sauvegarder ce fichier
dans un endroit où il ne sera pas effacé/perdu. C'est un **fichier important** qu'il ne faut **pas perdre** et qu'il ne faut
**surtout pas partager**.
4. Copier tout le texte dans le bloc `Public key for pasting into OpenSSH authorized_keys file`. Pour s'assurer
d'avoir tout bien copié, vérifier lors du collage que le texte commence bien par `ssh-rsa` et se termine bien par
la valeur rentrée dans `Key comment` (ici `your-nickname` sur la capture d'écran). Il faut ensuite créer un fichier
`cle_publique.txt` à côté de `cle_privee.ppk` et coller le texte dans ce fichier `cle_publique.txt`.
Tout comme `cle_privee.ppk`, il est important de ne **pas perdre** ce fichier. En revanche, il peut être partagé
(c'est même son but).

Une fois les fichiers enregistrés, fermer PuTTYgen puis contacter @Maxchaos sur le Discord des Serruriers. Il faudra
fournir 2 éléments :  
* La clé publique (`cle_publique.txt`)
* Le serveur auquel vous souhaitez accéder (GMod TTT, GMod minigames, etc...)

Si tout est OK, vous obtiendrez en retour un *nom d'utilisateur* à conserver pour la suite de ce tutoriel.

## 3- Paramétrage initial des logiciels

Il est nécessaire d'avoir terminé la partie 2 de ce tutoriel (génération de clés SSH + obtention d'un nom d'utilisateur)
pour réaliser cette partie 3.

### PuTTY

![putty_setup_01](img/putty_setup_01.png)  
Lancer PuTTY, le mieux est de rechercher `putty` dans la barre de recherche Windows 10 et de sélectionner PuTTY
dans les résultats. Sinon il est toujours possible d'aller chercher le programme dans la liste des programmes installés.

![putty_setup_02](img/putty_setup_02.png)  
Une fois PuTTY démarré :  
1. Rentrer l'adresse `91.121.83.216` dans la case `Host Name (or IP address)`
2. Vérifier que `22` est bien renseigné dans la case `Port`
3. Vérifier que SSH est bien sélectionné dans la partie `Connection type`

![putty_setup_03](img/putty_setup_03.png)  
Ensuite :  
1. Cliquer sur `Data`
2. Rentrer le nom d'utilisateur obtenu à la fin de la partie 2 de ce tutoriel dans la case `Auto-login username`

![putty_setup_04](img/putty_setup_04.png)  
1. Cliquer sur le `+` à côté de `SSH` pour dérouler les options associées
2. Cliquer sur `Auth`, pas sur le `+` à côté, mais sur le mot
3. Cliquer sur `Browse` dans la partie `Private key file for authentication` et aller chercher votre fichier `cle_privee.ppk`

![putty_setup_05](img/putty_setup_05.png)  
1. Cliquer sur `Session`
2. Rentrer le nom de votre choix dans `Saved Sessions`, c'est le nom de votre sauvegarde de tous les paramétrages qui
viennent d'être effectués donc choisissez quelque chose que vous retrouverez facilement.
3. Cliquer sur `Save`, le nom rentré précédemment devrait apparaître dans la liste en dessous de `Default Settings`.

Si tout est OK, fermer PuTTY, nous allons vérifier que la sauvegarde fonctionne.

![putty_setup_06](img/putty_setup_06.png)  
Lancer PuTTY:  
1. Cliquer sur le nom sauvegardé à l'étape précédente
2. Cliquer sur `Load`, cela va charger tous les paramètres enregistrés dans les précédentes étapes sans avoir besoin de tout refaire
3. Cliquer sur `Open`, un terminal va s'ouvrir et va demander de rentrer le mot de passe de votre clé si vous en avez paramétré un.
Lorsqu'on rentre le mot de passe, il est normal que rien ne s'affiche, c'est comme ça que cela fonctionne dans un terminal.
Rentrer le mot de passe s'il y en a un de demandé et appuyer sur la touche entrée. Vous devriez être sur la machine !

Informations additionnelles :  
Il est possible que lors de votre première connexion, PuTTY mette un avertissement en vous demandant de faire confiance au serveur
auquel vous vous connectez, acceptez de toujours faire confiance à la machine pour ne plus avoir ces avertissements.

### FileZilla

![filezilla_setup_01](img/filezilla_setup_01.png)  
Lancer FileZilla :  
1. Cliquer sur le logo de serveurs en haut à gauche
2. Cliquer sur `Nouveau Site`
3. Rentrer le nom de votre choix, un nom que vous comprenez et que vous pourrez retrouver facilement
4. Choisir `SFTP - SSH File Transfer Protocol` dans `Protocole`
5. Rentrer l'adresse `91.121.83.216` dans la case `Hôte`
6. Rentrer `22` dans la case `Port`
7. Choisir `Fichier de clé` dans `Type d'authentification`
8. Rentrer le nom d'utilisateur obtenu à la fin de la partie 2 de ce tutoriel dans la case `Identifiant`
9. Cliquer sur `Parcourir` dans la partie `Fichier de clé` et aller chercher votre fichier `cle_privee.ppk`
10. Cliquer sur `OK`

![filezilla_setup_02](img/filezilla_setup_02.png)  
1. Cliquer sur la flèche à côté du logo de serveurs en haut à gauche
2. Cliquer sur le nom enregistré précédemment qui doit normalement s'afficher. Si vous avez paramétré un mot de passe sur votre clé,
FileZilla va le réclamer à ce moment là. Rentrer le mot de passe et valider. Vous devriez être sur la machine !

Informations additionnelles :  
Il est possible que lors de votre première connexion, FileZilla mette un avertissement en vous demandant de faire confiance au serveur
auquel vous vous connectez, acceptez de toujours faire confiance à la machine pour ne plus avoir ces avertissements.
